/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.traxel.lumbermill.event;

import java.io.IOException;
import java.io.StringReader;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author Arnaud Fonce <arnaud.fonce@r-w-x.net>
 */
public class EventListenerXMLHandler extends DefaultHandler {

    private static final Logger logger = Logger.getLogger(EventListenerXMLHandler.class.getName());
    
    private final EventListener listener;
    
    private LogRecord record;
    private String currentValue;

    public EventListenerXMLHandler(EventListener listener) {
        this.listener = listener;
    }
            
    
    @Override
    public InputSource resolveEntity(String publicId, String systemId) throws IOException, SAXException {
        return new InputSource(new StringReader(""));
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        currentValue = "";
        if( qName.equals("record") ) {
            record = new LogRecord(Level.ALL, "");
        }
    }
    
    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        currentValue = currentValue + new String(ch, start, length);
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
//        System.out.println("END ELEMENT - " + qName + " - " + currentValue);
        if( qName.equals("message")) {
            record.setMessage(currentValue);
        }else if(qName.equals("sequence")) {
            record.setSequenceNumber(Long.parseLong(currentValue));
        }else if(qName.equals("logger")) {
            record.setLoggerName(currentValue);
        }else if(qName.equals("level")) {
            record.setLevel(Level.parse(currentValue));
        }else if(qName.equals("class")) {
            record.setSourceClassName(currentValue);
        }else if(qName.equals("method")) {
            record.setSourceMethodName(currentValue);
        }else if(qName.equals("thread")) {
            record.setLongThreadID(Long.parseLong(currentValue));
        }else if(qName.equals("record")) {
            listener.add(Event.create(record));
            record = null;
        }
    }

    
    
    
}
