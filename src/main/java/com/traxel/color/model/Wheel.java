package com.traxel.color.model;

import java.awt.Color;
import java.util.Map;

public abstract class Wheel {
  private final Map _colors;
  
  public Wheel(Map colors) {
    this._colors = colors;
    for (int i = 0; i < 360; i++)
      getBaseColor(i); 
  }
  
  public Color getBaseColor(int degrees) {
    degrees %= 360;
    Color color = (Color)this._colors.get(new Integer(degrees));
    if (color != null)
      return color; 
    int leftDegrees = degrees / 30 * 30;
    int rightDegrees = leftDegrees + 30;
    int leftDistance = degrees % 30;
    int leftParts = 30 - leftDistance;
    int rightParts = 30 - leftParts;
    Color leftColor = getBaseColor(leftDegrees);
    Color rightColor = getBaseColor(rightDegrees);
    color = Colors.blend(leftParts, leftColor, rightParts, rightColor);
    this._colors.put(new Integer(degrees), color);
    return color;
  }
  
  public Color getBaseColor(Color base, int degreesOffset) {
    for (int i = 0; i < 360; i++) {
      Color mapColor = getBaseColor(i);
      if (mapColor.equals(base))
        return getBaseColor(i + degreesOffset); 
    } 
    return null;
  }
}
