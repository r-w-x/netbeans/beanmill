package com.traxel.color.model;

import java.awt.Color;

public abstract class Colors {
  public static String hex(Color color) {
    return zeroPad(Integer.toHexString(color.getRed())) + zeroPad(Integer.toHexString(color.getGreen())) + zeroPad(Integer.toHexString(color.getBlue()));
  }
  
  private static String zeroPad(String string) {
    if (string == null)
      return "00"; 
    if (string.length() == 0)
      return "00"; 
    if (string.length() == 1)
      return "0" + string; 
    return string;
  }
  
  public static Color blend(double xParts, Color xC, double yParts, Color yC) {
    int r = blend(xParts, xC.getRed(), yParts, yC.getRed());
    int g = blend(xParts, xC.getGreen(), yParts, yC.getGreen());
    int b = blend(xParts, xC.getBlue(), yParts, yC.getBlue());
    return new Color(r, g, b);
  }
  
  private static int blend(double xParts, int xValue, double yParts, int yValue) {
    double totalParts = xParts + yParts;
    double xFactor = xValue * xParts;
    double yFactor = yValue * yParts;
    double result = (xFactor + yFactor) / totalParts;
    return (int)Math.round(result);
  }
}
