package com.traxel.color.model;

import java.awt.Color;
import java.util.HashMap;
import java.util.Map;

public class ScreenWheel extends Wheel {
  public static final Map COLORS = new HashMap();
  
  static {
    int mix = 255;
    Color blueGreen = new Color(0, mix, mix);
    Color redGreen = new Color(mix, mix, 0);
    Color redBlue = new Color(mix, 0, mix);
    COLORS.put(new Integer(0), Colors.blend(1.0D, redBlue, 1.0D, Color.BLUE));
    COLORS.put(new Integer(30), Color.BLUE);
    COLORS.put(new Integer(60), Colors.blend(1.0D, blueGreen, 1.0D, Color.BLUE));
    COLORS.put(new Integer(90), blueGreen);
    COLORS.put(new Integer(120), Colors.blend(1.0D, blueGreen, 1.0D, Color.GREEN));
    COLORS.put(new Integer(150), Color.GREEN);
    COLORS.put(new Integer(180), Colors.blend(1.0D, redGreen, 1.0D, Color.GREEN));
    COLORS.put(new Integer(210), redGreen);
    COLORS.put(new Integer(240), Colors.blend(1.0D, redGreen, 1.0D, Color.RED));
    COLORS.put(new Integer(270), Color.RED);
    COLORS.put(new Integer(300), Colors.blend(1.0D, redBlue, 1.0D, Color.RED));
    COLORS.put(new Integer(330), redBlue);
  }
  
  public ScreenWheel() {
    super(COLORS);
  }
}
