package de.cismet.tools;

/**
 * StaticHtmlTools.
 *
 * @author thorsten.hell@cismet.de
 * @version $Revision$, $Date$
 */
public class StaticHtmlTools {

    /**
     * Converts the specified <code>String</code> into a <code>HTMLString</code>.
     *
     * @param string  <code>String</code>, which is going to get converted
     *
     * @return  <code>HTMLString</code>
     */
    public static String stringToHTMLString(final String string) {
        if (string == null) {
            return null;
        }
        final StringBuffer sb = new StringBuffer(string.length());
        // true if last char was blank
        boolean lastWasBlankChar = false;
        final int len = string.length();
        char c;

        for (int i = 0; i < len; i++) {
            c = string.charAt(i);
            if (c == ' ') {
                // blank gets extra work,
                // this solves the problem you get if you replace all
                // blanks with &nbsp;, if you do that you loss
                // word breaking
                if (lastWasBlankChar) {
                    lastWasBlankChar = false;
                    sb.append("&nbsp;"); // NOI18N
                } else {
                    lastWasBlankChar = true;
                    sb.append(' ');
                }
            } else {
                lastWasBlankChar = false;
                //
                // HTML Special Chars
                if (c == '"') {          // NOI18N
                    sb.append("&quot;"); // NOI18N
                } else if (c == '&') {   // NOI18N
                    sb.append("&amp;");  // NOI18N
                } else if (c == '<') {   // NOI18N
                    sb.append("&lt;");   // NOI18N
                } else if (c == '>') {   // NOI18N
                    sb.append("&gt;");   // NOI18N
                } else if (c == '\n') {  // NOI18N
                    // Handle Newline
                    // sb.append("&lt;/br&gt;");
                    sb.append("\n"); // NOI18N
                } else {
                    final int ci = 0xffff & c;
                    if (ci < 160) {
                        // nothing special only 7 Bit
                        sb.append(c);
                    } else {
                        // Not 7 Bit use the unicode system
                        sb.append("&#"); // NOI18N
                        sb.append(new Integer(ci).toString());
                        sb.append(';');  // NOI18N
                    }
                }
            }
        }
        return sb.toString();
    }
}
