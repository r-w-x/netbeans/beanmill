
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    
    <groupId>de.cismet</groupId>
    <artifactId>beanmill</artifactId>
    <version>2.7-SNAPSHOT</version>
    <packaging>nbm</packaging>

    <name>Beanmill Logging Viewer (Log4J, JSR47)</name>
    <description>A logging viewer with a tight integration into Netbeans.</description>
    <licenses>
        <license>
            <name>GNU General Public License</name>
            <distribution>repo</distribution>
            <url>http://www.gnu.org/licenses/gpl.html</url>
        </license>
    </licenses>

    <developers>
        <developer>
            <name>Arnaud Fonce</name>
            <email>arnaud.fonce@r-w-x.net</email>
            <url>http://blog.r-w-x.net</url>
            <organization>Room Work eXperience</organization>
            <roles>
                <role>developer</role>
            </roles>
            <timezone>France/Paris</timezone>
        </developer>
        <developer>
            <id>thell</id>
            <name>Thorsten Hell</name>
            <email>thorsten.hell@cismet.de</email>
            <organization>cismet GmbH</organization>
        </developer>
        <developer>
            <id>mscholl</id>
            <name>Martin Scholl</name>
            <email>martin.scholl@cismet.de</email>
            <organization>cismet GmbH</organization>
        </developer>
    </developers>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <netbeans.version>RELEASE110</netbeans.version>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
    </properties>
    
    <dependencies>
        <dependency>
            <groupId>org.netbeans.api</groupId>
            <artifactId>org-netbeans-api-annotations-common</artifactId>
            <version>${netbeans.version}</version>
        </dependency>
        <dependency>
            <groupId>org.netbeans.api</groupId>
            <artifactId>org-openide-util</artifactId>
            <version>${netbeans.version}</version>
        </dependency>
        <dependency>
            <groupId>org.netbeans.api</groupId>
            <artifactId>org-openide-util-lookup</artifactId>
            <version>${netbeans.version}</version>
        </dependency>
        <dependency>
            <groupId>org.netbeans.api</groupId>
            <artifactId>org-openide-modules</artifactId>
            <version>${netbeans.version}</version>
        </dependency>
        <dependency>
            <groupId>org.netbeans.api</groupId>
            <artifactId>org-openide-windows</artifactId>
            <version>${netbeans.version}</version>
        </dependency>
        <dependency>
            <groupId>org.netbeans.api</groupId>
            <artifactId>org-openide-filesystems</artifactId>
            <version>${netbeans.version}</version>
        </dependency>
        <dependency>
            <groupId>org.netbeans.api</groupId>
            <artifactId>org-netbeans-api-java-classpath</artifactId>
            <version>${netbeans.version}</version>
        </dependency>
        <dependency>
            <groupId>org.netbeans.api</groupId>
            <artifactId>org-openide-loaders</artifactId>
            <version>${netbeans.version}</version>
        </dependency>
        <dependency>
            <groupId>org.netbeans.api</groupId>
            <artifactId>org-netbeans-modules-editor</artifactId>
            <version>${netbeans.version}</version>
        </dependency>
        <dependency>
            <groupId>org.netbeans.api</groupId>
            <artifactId>org-netbeans-modules-options-api</artifactId>
            <version>${netbeans.version}</version>
        </dependency>
        <dependency>
            <groupId>org.netbeans.api</groupId>
            <artifactId>org-openide-awt</artifactId>
            <version>${netbeans.version}</version>
        </dependency>
        <dependency>
            <groupId>org.netbeans.api</groupId>
            <artifactId>org-openide-nodes</artifactId>
            <version>${netbeans.version}</version>
        </dependency>
        <dependency>
            <groupId>org.netbeans.api</groupId>
            <artifactId>org-openide-text</artifactId>
            <version>${netbeans.version}</version>
        </dependency>
        <dependency>
            <groupId>org.netbeans.api</groupId>
            <artifactId>org-openide-util-ui</artifactId>
            <version>${netbeans.version}</version>
        </dependency>
        <dependency>
            <groupId>log4j</groupId>
            <artifactId>log4j</artifactId>
            <version>1.2.12</version>
        </dependency>
        <dependency>
            <groupId>org.jdom</groupId>
            <artifactId>jdom2</artifactId>
            <version>2.0.6</version>
        </dependency>
        <dependency>
            <groupId>org.swinglabs</groupId>
            <artifactId>swing-layout</artifactId>
            <version>1.0.3</version>
        </dependency>
    </dependencies>


    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.netbeans.utilities</groupId>
                <artifactId>nbm-maven-plugin</artifactId>
                <version>4.6</version>
                <extensions>true</extensions>
                <configuration>
                    <moduleType>normal</moduleType>
                    <codeNameBase>de.cismet.beanmill/1</codeNameBase>
                    <licenseName>GPL</licenseName>
                    <licenseFile>${basedir}/src/main/license/gpl-3.0.txt</licenseFile>
                    <homePageUrl>http://github.com/cismet/beanmill</homePageUrl>
                    <keystore>${de.cismet.keystore.path}</keystore>
                    <keystorealias>cismet</keystorealias>
                    <keystorepassword>${de.cismet.keystore.pass}</keystorepassword>
                    <!-- 
                        Hmm, http://mojo.codehaus.org/nbm-maven/nbm-maven-plugin/index.html states that packages are
                        private by default and that you have to explicitely declare public packages while
                        http://bits.netbeans.org/dev/javadoc/org-openide-modules/org/openide/modules/doc-files/classpath.html#public-packages
                        says that everything is public by default if not declared otherwise. Thus we explicitely create
                        two entries for the public packages which are empty, this one and the one in the manifest.
                    -->
                    <publicPackages>
                    </publicPackages>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-jar-plugin</artifactId>
                <version>2.6</version>
                <configuration>
                    <!-- to have the jar plugin pickup the nbm generated manifest -->
                    <useDefaultManifestFile>true</useDefaultManifestFile>
                </configuration>
            </plugin>
        </plugins>
    </build>
    
    
</project>
